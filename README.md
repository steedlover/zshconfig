# Installation

```bash
git clone https://bitbucket.org/steedlover/zshconfig.git ~/zsh
cp ~/zsh/.zshrc ~/
```

> Make sure you have installed the NerdFont from the [vimhome repository](https://bitbucket.org/steedlover/vimhome/src/modern/)

### Ubuntu

**Variables and apt packages**

```bash
sudo apt install zsh neofetch
```

### MacOS

```
brew install zsh neofetch
```

**Reload the config**

```bash
source ~/.zshrc
```

**OhMyZsh**

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

> Keep in mind on this step a default `.oh-my-zsh` config file will be installed  
> So don't forget to restore one from the repository

**Zsh plugins**

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
```

**Powerlevel10K theme**

```bash
git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
```

**Set up ZSH as a default shell**

```bash
chsh -s $(which zsh)
```
